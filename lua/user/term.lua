local M = {}

local Terminal = require("toggleterm.terminal").Terminal

local git_cz = "git cz"
local git_add_com = "git add --all"
local git_push_com = "git push"
local git_status_com = "git status"

local git_commit = Terminal:new({
	cmd = git_cz,
	dir = "git_dir",
	hidden = true,
	direction = "float",
	float_opts = {
		border = "double",
	},
})

local git_add = Terminal:new({
	cmd = git_add_com,
	dir = "git_dir",
	hidden = true,
	direction = "float",
	float_opts = {
		border = "double",
	},
})

local git_push = Terminal:new({
	cmd = git_push_com,
	dir = "git_dir",
	hidden = true,
	direction = "float",
	float_opts = {
		border = "double",
	},
})

local git_status = Terminal:new({
	cmd = git_status_com,
	dir = "git_dir",
	hidden = false,
	direction = "float",
	float_opts = {
		border = "double",
	},
})

function M.git_commit_toggle()
	git_commit:toggle()
end

function M.git_add_all_toggle()
	git_add:toggle()
end

function M.git_push_toggle()
	git_push:toggle()
end

function M.git_status_toggle()
	git_status:toggle()
end

return M
